class Question < ApplicationRecord
  validates :title, presence: true, length: { minimum: 3 }
  validates :body, presence: true, length: { minimum: 3 }

  def time
    created_at.strftime('%Y-%m-%d %H:%M:%S')
  end
end
